#ifndef SCSTRUCTS_H
#define SCSTRUCTS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "errors.h"
#include "dots.h"

#define CRMAX 255
#define MSTEP 1
#define RSTEP 0.05
#define COS_RSTEP 0.99875026
#define SIN_RSTEP 0.04997917

typedef struct {
    unsigned int r, g, b;
} color_t;

typedef struct {
	size_t a, b, c;
    dot3f_t v0, n, e1, e2;
} poly3_t;

typedef struct {
	size_t dots_n, poly_n;
	dot3f_t *dots;
	poly3_t *polygones;
    color_t color;
    float diff_k, spec_k, spec_exp, reflect_k, transparency_k;
    float refract_r, refract_g, refract_b;
} cryst_t;

typedef struct {
	float intesity;
	dot3f_t pos;
    int inf;
} lsrc_t;

typedef struct {
    size_t cryst_n, lsrc_n, rec_depth;
	cryst_t *crystalls;
	lsrc_t *lsources;
    color_t bg_color;
} scene_t;

typedef struct {
    dot3f_t x, y, z;
} base_t;

typedef struct {
	float fov;
    dot3f_t pos;
    base_t dir;
} cam_t;

typedef struct {
    size_t width, height;
	cam_t camera;
} view_t;

typedef struct {
    size_t width, height;
    size_t stride;
    uint8_t *pixs;
} image_t;

int create_scene(const char *filename, scene_t *scene);
void destroy_scene(scene_t *scene);

int setup_view(const char *filename, view_t *view);

int upload_img(image_t *image, char *filename);
void setup_image(image_t *image, size_t width, size_t height, uint8_t *buf, size_t stride);

void print_color(FILE *file, color_t color);

void move_cam(cam_t *cam, dot3f_t delta);
void rotate_camx(cam_t *cam, float delta);
void rotate_camy(cam_t *cam, float delta);
void rotate_camz(cam_t *cam, float delta);

static inline void set_intense(color_t *color, float intese)
{
    color->b *= intese;
    color->g *= intese;
    color->r *= intese;
}

static inline void add_color(color_t *c1, color_t *c2)
{
    c1->r += c2->r;
    c1->g += c2->g;
    c1->b += c2->b;
}

static inline void normalize_color(color_t *color)
{
    unsigned int m = color->b;
    if (color->g > m)
        m = color->g;
    if (color->r > m)
        m = color->r;

    if (color->r + color->g + color->b > CRMAX * 3)
        color->r = color->g = color->b = CRMAX;
    else if (m > CRMAX)
        color->r = (unsigned int)((float)color->r * 255.0 / (float)m),
        color->g = (unsigned int)((float)color->g * 255.0 / (float)m),
        color->b = (unsigned int)((float)color->b * 255.0 / (float)m);
}

#endif
