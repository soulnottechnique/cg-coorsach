#include "tracer.h"

static void calc_rdir(cam_t cam, float n, float m,
                      dot3f_t *ki, dot3f_t *kj, dot3f_t *kc)
{
    *ki = cam.dir.y, *kj = cam.dir.x, *kc = cam.dir.z;

    dot3f_t mt = cam.dir.y, nt = cam.dir.x;

    mul3f(&mt, (float)m / 2.0);
    mul3f(&nt, (float)n / 2.0);

    mul3f(ki, (float)m / ((float)m - 1));
    mul3f(kj, (float)n / ((float)n - 1));
    mul3f(kc, (float)m / (2 * tanf(cam.fov / 360.0 * 3.1415926)));

    *kc = sub3f(sub3f(*kc, mt), nt);
}

static inline bool triangle_intersection(dot3f_t orig, dot3f_t dir,
                            poly3_t *p, float *dist)
{
    dot3f_t e1 = p->e1;
    dot3f_t e2 = p->e2;

    dot3f_t pvec = cross3f(dir, e2);
    float det = dot3f(e1, pvec);

    if (det < 1e-8 && det > -1e-8)
        return false;

    float inv_det = 1 / det;
    dot3f_t tvec = sub3f(orig, p->v0);
    float u = dot3f(tvec, pvec) * inv_det;
    if (u < 0 || u > 1)
        return false;

    dot3f_t qvec = cross3f(tvec, e1);
    float v = dot3f(dir, qvec) * inv_det;
    if (v < 0 || u + v > 1)
        return false;

    *dist = dot3f(e2, qvec) * inv_det;
    return true;
}

inline static dot3f_t reflect(const dot3f_t i, const dot3f_t n)
{
    dot3f_t tvec = n;
    mul3f(&tvec, dot3f(i, n) * 2.0);
    return sub3f(i, tvec);
}

inline static dot3f_t refract(dot3f_t i, dot3f_t n, float ri, bool into)
{
    float dot = -dot3f(i, n);

    if (dot > 1.0)
        dot = 1.0;
    else if (dot < -1.0)
        dot = -1.0;

    if (into)
        ri = 1.0 / ri;

    if (dot < 0)
    {
        dot = -dot;
        n = neg3f(n);
    }

    float k = 1 - ri * ri * (1.0 - dot * dot);

    if (k < 0)
        return (dot3f_t){0, 0, 0};

    mul3f(&i, ri);
    mul3f(&n, ri * dot - sqrtf(k));
    return add3f(i, n);
}

static bool scene_intersection(size_t *cr_id, size_t *p_id, float *dst,
                               dot3f_t orig, dot3f_t dir, const scene_t *scene)
{
    cryst_t cry;
    bool nfound = true;
    float dist;

    for (size_t i = 0; i < scene->cryst_n; i++)
    {
        cry = scene->crystalls[i];
        poly3_t *pg = cry.polygones;

        for (size_t p = 0; p < cry.poly_n; p++)
        {

            if (triangle_intersection(orig, dir, pg + p, &dist) && dist > 0)
            {
                if (nfound || dist < *dst)
                {
                    nfound = false;
                    *dst = dist;
                    *cr_id = i;
                    *p_id = p;
                }
            }
        }
    }

    return !nfound;
}

dot3f_t offset_orig(dot3f_t orig, dot3f_t n, dot3f_t side, float eps)
{
    float d = dot3f(side, n);

    mul3f(&n, eps);
    if (d > 0)
        return add3f(orig, n);

    return sub3f(orig, n);
}

color_t cast_ray(dot3f_t orig, dot3f_t dir, scene_t *scene, size_t d, bool into)
{
    float mdst, tdst;
    size_t cr_id, p_id, tcr;
    color_t rcolor = scene->bg_color;
    dot3f_t res;
    cryst_t cry;

    normalize3f(&dir);
    bool found = scene_intersection(&cr_id, &p_id, &mdst,
                                    orig, dir, scene);

    if (d > scene->rec_depth || !found)
        return rcolor;

    cry = scene->crystalls[cr_id];
    rcolor = cry.color;

    res = dir;
    mul3f(&res, mdst);
    res = add3f(res, orig);

    dot3f_t n = cry.polygones[p_id].n;

    if (dot3f(n, dir) > 0)
        n = neg3f(n);

    float diff_inten = 0.0, spec_inten =  0.0;
    float l_dist;

    dot3f_t reflect_dir = reflect(dir, n);
    normalize3f(&reflect_dir);
    dot3f_t reflect_orig = offset_orig(res, n, reflect_dir, 1e-3);
    color_t reflect_color = cast_ray(reflect_orig, reflect_dir, scene, d + 1, into);
    set_intense(&reflect_color, dot3f(reflect_dir, n));

    dot3f_t rd_r = refract(dir, n, cry.refract_r, into);
    normalize3f(&rd_r);

    dot3f_t rd_g = refract(dir, n, cry.refract_g, into);
    normalize3f(&rd_g);

    dot3f_t rd_b = refract(dir, n, cry.refract_b, into);
    normalize3f(&rd_b);

    dot3f_t refract_orig = offset_orig(res, n, rd_g, 1e-3);

    color_t refract_color;
    refract_color.r = cast_ray(refract_orig, rd_r, scene, d + 1, !into).r;
    refract_color.g = cast_ray(refract_orig, rd_g, scene, d + 1, !into).g;
    refract_color.b = cast_ray(refract_orig, rd_b, scene, d + 1, !into).b;

    for (size_t i = 0; i < scene->lsrc_n; i++)
    {
        lsrc_t lsrc = scene->lsources[i];
        dot3f_t lvec = lsrc.inf ? lsrc.pos : sub3f(lsrc.pos, res);

        l_dist = norm3f(lvec);
        normalize3f(&lvec);

        dot3f_t shadow_orig = offset_orig(res, n, lvec, 1e-3);

        if (scene_intersection(&tcr, &p_id, &tdst, shadow_orig, lvec, scene))
        {
            mul3f(&lvec, tdst);
            if (norm3f(lvec) < l_dist)
                continue;
        }

        float ri = dot3f(lvec, n);
        if (ri > 0)
            diff_inten += ri * lsrc.intesity;

        ri = dot3f(reflect(lvec, n), dir);
        if (ri > 0)
            spec_inten += powf(ri, cry.spec_exp)*lsrc.intesity;
    }

    color_t c1 = {255, 255, 255};

    set_intense(&rcolor, diff_inten * cry.diff_k);
    set_intense(&c1, spec_inten * cry.spec_k);
    set_intense(&reflect_color, cry.reflect_k);
    set_intense(&refract_color, cry.transparency_k);
    add_color(&rcolor, &c1);
    add_color(&rcolor, &reflect_color);
    add_color(&rcolor, &refract_color);
    normalize_color(&rcolor);

    return rcolor;
}

int cpu_trace(scene_t *scene, view_t *view, image_t *image)
{
    size_t m = image->width;
    size_t n = image->height;
    cam_t cam = view->camera;

    dot3f_t ki, kj, kc;
    calc_rdir(cam, n, m, &ki, &kj, &kc);

    //#pragma omp parallel for num_threads(32)
    for (size_t i = 0; i < n; i++)
    {
        uint8_t *pix = image->pixs + i * image->stride;
        dot3f_t dir = ki;
        mul3f(&dir, i);
        dir = add3f(dir, kc);

        for (size_t j = 0; j < m; j++)
        {
            dot3f_t t = kj;
            mul3f(&t, j);
            uint8_t *tpix = pix + 4 * j;

            color_t col = cast_ray(cam.pos, add3f(dir, t), scene, 0, true);
            normalize_color(&col);
			
            tpix[0] = col.b, tpix[1] = col.g, tpix[2] = col.r;
        }
    }

    return EXIT_SUCCESS;
}
