#ifndef TRACER_H
#define TRACER_H

#include <stdlib.h>
#include <math.h>

#include "scstructs.h"
#include "stdbool.h"
#include "stdint.h"

int cpu_trace(scene_t *scene, view_t *view, image_t *image);


#endif
