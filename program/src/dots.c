#include "dots.h"

int load_dot3f(FILE *file, dot3f_t *dot)
{
	if (fscanf(file, "%f %f %f", &dot->x, &dot->y, &dot->z) != 3)
		return EXIT_FAILURE;
	return EXIT_SUCCESS;
}

int load_dot2f(FILE *file, dot2f_t *dot)
{
	if (fscanf(file, "%f %f", &dot->x, &dot->y) != 2)
		return EXIT_FAILURE;
	return EXIT_SUCCESS;
}


