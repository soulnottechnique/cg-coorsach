#include <stdlib.h>
#include <stdio.h>

#include "scstructs.h"
#include "tracer.h"

int main(int argc, char *argv[])
{	
	scene_t scene;
	view_t view;
    image_t image;
	
	setbuf(stdout, NULL);
	
	if (argc != 4)
		return 1;
		
    int rc = create_scene(argv[1], &scene);
	
	if (rc == EXIT_SUCCESS)
	{
        rc = setup_view(argv[2], &view);
		
		if (rc != EXIT_SUCCESS)
		{
			destroy_scene(&scene);
			return 2;
		}
		
		uint8_t *imgbuf = malloc(view.width * view.height * 4 * sizeof(uint8_t));
		
		if (!imgbuf)
		{
			destroy_scene(&scene);
			return 3;
		}
		
		setup_image(&image, view.width, view.height, imgbuf, view.width * 4);
		
        cpu_trace(&scene, &view, &image);
		
        upload_img(&image, argv[3]);
		
		free(imgbuf);
		destroy_scene(&scene);

        return EXIT_SUCCESS;
	}
	
    return 4;
}
