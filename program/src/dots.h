#ifndef DOTS_H
#define DOTS_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
	float x, y;
} dot2f_t;

typedef struct {
	float x, y, z;
} dot3f_t;

int load_dot3f(FILE *file, dot3f_t *dot);
int load_dot2f(FILE *file, dot2f_t *dot);

void rotate3f(dot3f_t *vec, dot3f_t delta);

static inline dot3f_t sub3f(dot3f_t a, dot3f_t b)
{
    return (dot3f_t){a.x - b.x, a.y - b.y, a.z - b.z};
}

static inline dot3f_t add3f(dot3f_t a, dot3f_t b)
{
    return (dot3f_t){a.x + b.x, a.y + b.y, a.z + b.z};
}

static inline dot3f_t cross3f(dot3f_t a, dot3f_t b)
{
    return (dot3f_t){a.y * b.z - a.z * b.y,
                     a.z * b.x - a.x * b.z,
                     a.x * b.y - a.y * b.x};
}

static inline float dot3f(dot3f_t a, dot3f_t b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

static inline void mul3f(dot3f_t *dot, float k)
{
    dot->x *= k, dot->y *= k, dot->z *= k;
}

static inline void normalize3f(dot3f_t *dot)
{
    float l = sqrt(dot->x * dot->x + dot->y * dot->y + dot->z * dot->z);
    if (l > 1e-8)
        dot->x /= l, dot->y /= l, dot->z /= l;
}

static inline dot3f_t neg3f(dot3f_t dot)
{
    return (dot3f_t){-dot.x, -dot.y, -dot.z};
}

static inline float norm3f(dot3f_t dot)
{
    return dot.x * dot.x + dot.y * dot.y + dot.z * dot.z;
}

static inline void rotate3fx(dot3f_t *vec, float delta)
{
    float cosx = cosf(delta), sinx = sinf(delta);

    float tx = vec->y;
    vec->y = vec->y * cosx - vec->z * sinx;
    vec->z = tx * sinx + vec->z * cosx;
}


static inline void rotate3fy(dot3f_t *vec, float delta)
{
    float cosy = cosf(delta), siny = sinf(delta);

    float tx = vec->x;
    vec->x = vec->x * cosy + vec->z * siny;
    vec->z = vec->z * cosy - tx * siny;
}

static inline void rotate3fz(dot3f_t *vec, float delta)
{
    float cosz = cosf(delta), sinz = sinf(delta);

    float tx = vec->x;
    vec->x = vec->x * cosz - vec->y * sinz;
    vec->y = tx * sinz + vec->y * cosz;
}

#endif
