#include "scstructs.h"

static int create_cryst(FILE *file, cryst_t *cryst);
static void destroy_cryst(cryst_t *cryst);
static int load_lsrc(FILE *file, lsrc_t *lsrc);
static int load_poly3(FILE *file, poly3_t *poly);
static int load_cam(FILE *file, cam_t *cam);
static int load_base(FILE *file, base_t *base);
static int load_color(FILE *file, color_t *color);
static void init_polys3(poly3_t *polys, const dot3f_t *dots, size_t pn);

int setup_view(const char *filename, view_t *view)
{
	int rc;
	
	FILE *file = fopen(filename, "r");
	if (!file)
		return OFILE_ERR;
	
    if (fscanf(file, "%zu %zu", &view->width, &view->height) != 2)
	{
		fclose(file);
		return EXIT_FAILURE;
	}
	
	if ((rc = load_cam(file, &view->camera)) != EXIT_SUCCESS)
	{
		fclose(file);
		return rc;
	}

	fclose(file);
	return EXIT_SUCCESS;
}

int create_scene(const char *filename, scene_t *scene)
{
	int rc;
	
	FILE *file = fopen(filename, "r");
	if (!file)
		return OFILE_ERR;
		
	
    if (fscanf(file, "%zu", &scene->rec_depth) != 1)
    {
        fclose(file);
        return EXIT_FAILURE;
    }

    if ((rc = load_color(file, &scene->bg_color)) != EXIT_SUCCESS)
    {
        fclose(file);
        return EXIT_FAILURE;
    }

    if (fscanf(file, "%zu %zu", &scene->cryst_n, &scene->lsrc_n) != 2)
	{
		fclose(file);
		return EXIT_FAILURE;
	}

    scene->crystalls = (cryst_t *)calloc(scene->cryst_n, sizeof(cryst_t));
    scene->lsources = (lsrc_t *)calloc(scene->lsrc_n, sizeof(lsrc_t));
	
	if (!scene->crystalls || !scene->lsources)
	{
		fclose(file);
		destroy_scene(scene);
		return ALLOC_ERR;
	}

	for (size_t i = 0; i < scene->cryst_n; i++)
		if ((rc = create_cryst(file, scene->crystalls + i)) != EXIT_SUCCESS)
		{
			fclose(file);
			destroy_scene(scene);
			return rc;
		}

	for (size_t i = 0; i < scene->lsrc_n; i++)
		if ((rc = load_lsrc(file, scene->lsources + i)) != EXIT_SUCCESS)
		{
			fclose(file);
			destroy_scene(scene);
			return rc;
		}

	fclose(file);

	return EXIT_SUCCESS;
}

void destroy_scene(scene_t *scene)
{
	if (scene)
	{
		if (scene->crystalls)
			for (size_t i = 0; i < scene->cryst_n; i++)
				destroy_cryst(scene->crystalls + i);
		
		free(scene->lsources);
		free(scene->crystalls);
	}
}

static int create_cryst(FILE *file, cryst_t *cryst)
{
	if (fscanf(file, "%zu %zu", &cryst->dots_n, &cryst->poly_n) != 2)
		return EXIT_FAILURE;

    if (load_color(file, &cryst->color) != EXIT_SUCCESS)
        return EXIT_FAILURE;

    if (fscanf(file, "%f %f %f %f", &cryst->diff_k, &cryst->spec_k, &cryst->spec_exp, &cryst->reflect_k) != 4)
        return EXIT_FAILURE;

    if (fscanf(file, "%f %f %f %f", &cryst->transparency_k, &cryst->refract_r, &cryst->refract_g, &cryst->refract_b) != 4)
        return EXIT_FAILURE;
	
    cryst->dots = (dot3f_t *)malloc(cryst->dots_n * sizeof(dot3f_t));
    cryst->polygones = (poly3_t *)malloc(cryst->poly_n * sizeof(poly3_t));
	
	if (!cryst->polygones || !cryst->dots)
	{
		destroy_cryst(cryst);
		return ALLOC_ERR;
	}
	
	for (size_t i = 0; i < cryst->dots_n; i++)
		if (load_dot3f(file, cryst->dots + i) != EXIT_SUCCESS)
		{
			destroy_cryst(cryst);
			return EXIT_FAILURE;
		}
	
	for (size_t i = 0; i < cryst->poly_n; i++)
		if (load_poly3(file, cryst->polygones + i) != EXIT_SUCCESS)
		{
			destroy_cryst(cryst);
			return EXIT_FAILURE;
		}

    init_polys3(cryst->polygones, cryst->dots, cryst->poly_n);

	return EXIT_SUCCESS;
}

static void destroy_cryst(cryst_t *cryst)
{
	if (cryst)
	{
		free(cryst->dots);
		free(cryst->polygones);
	}
}

static int load_cam(FILE *file, cam_t *cam)
{
	if (fscanf(file, "%f", &cam->fov) != 1)
		return EXIT_FAILURE;
	
	if (load_dot3f(file, &cam->pos) != EXIT_SUCCESS)
		return EXIT_FAILURE;
	
    if (load_base(file, &cam->dir) != EXIT_SUCCESS)
		return EXIT_FAILURE;
	
	return EXIT_SUCCESS;
} 

static int load_base(FILE *file, base_t *base)
{
    if (load_dot3f(file, &base->x) != EXIT_SUCCESS)
        return EXIT_FAILURE;

    if (load_dot3f(file, &base->y) != EXIT_SUCCESS)
        return EXIT_FAILURE;

    if (load_dot3f(file, &base->z) != EXIT_SUCCESS)
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}

static int load_lsrc(FILE *file, lsrc_t *lsrc)
{
	if (fscanf(file, "%f", &lsrc->intesity) != 1)
		{ return EXIT_FAILURE; }
	
	if (load_dot3f(file, &lsrc->pos) != EXIT_SUCCESS)
		{ return EXIT_FAILURE; }

    if (fscanf(file, "%d", &lsrc->inf) != 1)
        { return EXIT_FAILURE; }
	
	return EXIT_SUCCESS;
}

static int load_poly3(FILE *file, poly3_t *poly)
{
	if (fscanf(file, "%zu %zu %zu", &poly->a, &poly->b, &poly->c) != 3)
		return EXIT_FAILURE;
	return EXIT_SUCCESS;
}

static void init_polys3(poly3_t *polys, const dot3f_t *dots, size_t pn)
{
    for (size_t i = 0; i < pn; i++)
    {
        dot3f_t v0 = dots[polys[i].a], v1 = dots[polys[i].b], v2 = dots[polys[i].c];

        polys[i].v0 = v0;
        polys[i].e1 = sub3f(v1, v0);
        polys[i].e2 = sub3f(v2, v0);
        polys[i].n = cross3f(polys[i].e1, polys[i].e2);
        normalize3f(&polys[i].n);
    }
}

void setup_image(image_t *image, size_t width, size_t height, uint8_t *buf, size_t stride)
{
    image->height = height;
    image->width = width;
    image->stride = stride;
    image->pixs = buf;
}

void print_color(FILE *file, color_t color)
{
    fprintf(file, "%d %d %d\n", color.r, color.g, color.b);
}

static int load_color(FILE *file, color_t *color)
{
    if (fscanf(file, "%u %u %u", &color->r, &color->g, &color->b) != 3)
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}

void move_cam(cam_t *cam, dot3f_t delta)
{
    dot3f_t a = cam->dir.x, b = cam->dir.y, c = cam->dir.z;

    mul3f(&a, delta.x);
    mul3f(&b, delta.y);
    mul3f(&c, delta.z);

    cam->pos = add3f(cam->pos, a);
    cam->pos = add3f(cam->pos, b);
    cam->pos = add3f(cam->pos, c);
}

void rotate_camx(cam_t *cam, float delta)
{
    rotate3fx(&cam->dir.x, delta);
    rotate3fx(&cam->dir.y, delta);
    rotate3fx(&cam->dir.z, delta);
}

void rotate_camy(cam_t *cam, float delta)
{
    rotate3fy(&cam->dir.x, delta);
    rotate3fy(&cam->dir.y, delta);
    rotate3fy(&cam->dir.z, delta);
}

void rotate_camz(cam_t *cam, float delta)
{
    rotate3fz(&cam->dir.x, delta);
    rotate3fz(&cam->dir.y, delta);
    rotate3fz(&cam->dir.z, delta);
}

int upload_img(image_t *image, char *filename)
{
	size_t m = image->width;
    size_t n = image->height;
	
	FILE *file = fopen(filename, "w");
    if (!file)
        return OFILE_ERR;

    fprintf(file, "P3\n%zu %zu\n%d\n\n", m, n, CRMAX);
	
	for (size_t i = 0; i < n; i++)
    {
        uint8_t *pix = image->pixs + i * image->stride;

        for (size_t j = 0; j < m; j++)
        {
            uint8_t *tpix = pix + 4 * j;
            //tpix[0] = col.b, tpix[1] = col.g, tpix[2] = col.r;
			print_color(file, (color_t){tpix[2], tpix[1], tpix[0]});
        }
    }
	
	fclose(file);
	return EXIT_SUCCESS;
}

